<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Executive Status Report v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	*{ Brief description of the document }*



### Approval

​	*{ Approval clause for the document }*

| &nbsp; &nbsp; &nbsp; &nbsp; Business Analyst &nbsp; &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; Project Sponsor &nbsp; &nbsp; | Implmentation SME |
| :----------------------------------------------------------: | :-----------------------------------------: | :---------------: |
|                  &nbsp;<br /> &nbsp;<br />                   |                                             |                   |
|                        Juan Dela Cruz                        |               Juana Dela Cruz               |   John D Cruise   |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |  Juan Cruise   |  01/01/2019   |  Tom Juan   |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Executive Milestone Overview

| ID   | Executive Milestones | Status | Baseline Completion | Expected Completion | Degree of Confidence | Change? |
| ---- | ------------------------------------------------------------ | ------ | ------------------- | ------------------- | -------------------- | ------- |
|      | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |        |                     |                     |                      |         |
|      |                                                              |        |                     |                     |                      |         |
|      |                                                              |        |                     |                     |                      |         |



## Project Milestone Overview





## Planned Accomplishments Summary

| Planned accomplishments for the coming two weeks:      |
| ------------------------------------------------------ |
| • *{ accomplishment 1 }*<br />• *{ accomplishment 2 }* |



## Integration Milestone Status Review

### Milestones That Other Projects Are Dependent On

| ID   | Integration Milestone | Projects Dependent | Status | Baseline Date | Expected Date | Degree of Confidence | Change? |
| ---- | --------------------- | ------------------ | ------ | ------------- | ------------- | -------------------- | ------- |
|      |                       |                    |        |               |               |                      |         |
|      |                       |                    |        |               |               |                      |         |
|      |                       |                    |        |               |               |                      |         |




### Project or Integration Milestones from Other Projects That This Project Is Dependent On:

| ID   | Project | Integration Milestone | Expected Date | Complete | Integration Concerns |
| ---- | ------- | --------------------- | ------------- | -------- | -------------------- |
|      |         |                       |               |          |                      |
|      |         |                       |               |          |                      |
|      |         |                       |               |          |                      |



## Project Issues Summary

| ID   | Priority | Issue Description | Impact Summary | Action Steps |
| ---- | -------- | ----------------- | -------------- | ------------ |
|      |          |                   |                |              |
|      |          |                   |                |              |
|      |          |                   |                |              |



## Project Risk Summary

| ID   | Priority | Probability of Occurance | Risk Description | Impact Summary | Response Strategy |
| ---- | -------- | ------------------------ | ---------------- | -------------- | ----------------- |
|      |          |                          |                  |                |                   |
|      |          |                          |                  |                |                   |
|      |          |                          |                  |                |                   |



## Executive Assisstance Requests

| ID   | Description including reference to milestone impacted | Action Requested |
| ---- | ----------------------------------------------------- | ---------------- |
|      |                                                       |                  |
|      |                                                       |                  |
|      |                                                       |                  |



## Project Notes

​	*{ add text }*



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: Baseline Completion
[^2]: Expected Completion
[^3]: Degree of Confidence



