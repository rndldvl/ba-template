<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Business Analysis Plan v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The purpose of the business analysis plan is to define an appropriate method to conduct business analysis activities, define how decisions are made about requirements and designs, including reviews, change control, approvals, and prioritization



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Business Analysis Plan and agree with the approach it presents. Changes to this Business Analysis Plan will be coordinated with and approved by the undersigned or their designated representatives through the processess stated in this document. Upon approval and signing of the business analysis plan will render the contents of this document as the basis and protocol for all the business analysis work and outcomes it entails.

|      Project Sponsor      | Change Approver | Business Analyst |
| :-----------------------: | :-------------: | :--------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |                  |
|      Juan Dela Cruz       | Juana Dela Cruz |  John D Cruise   |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------


## Executive Summary

​	*{ Brief summary of the document. }*



------

## Business Analysis Approach

### Approach

​	*{ The business analysis approach deemed fit for the project is a **predictive** approach. For such and such reasons. }*



### Activities

​	*{ Description of the types of activities that the business analyst will perform. Frequently the organization’s adopted methodologies influence the activities that are selected. }*

- **The following are the activities that will be primarily used:**
  - Concept and Data Modelling
  - Interviews
  - Mind Mapping
  - Reviews
  - Use Cases and Scenarios
  - User Stories



### Timing

​	*{ Business analysts determine when the business analysis tasks need to be**performed and if the level of business analysis effort will need to vary over time. }*



### Deliverables

​	Listed below are the mandatory documents in a standard business analysis cycle. However, project circumstances might entail the creation of other business analysis documents.

   - Business Analysis Plan
   - Strategy Analysis
   - Elicitation Results
   - Requirements Analysis and Design
   - Requirements Traceability Matrix
   - Requirements Approval and Prioritization
   - Requirements Change Assessment
   - Solution Evaluation



### Complexity & Risks

​	*{ The complexity and size of the change and the overall risk of the effort to the organization are considered when determining the business analysis approach. }*



------

## Stakeholder Engagement Approach

### Stake Holders

- **Project Sponsor**
  - **Johnny Dela Kruz**
    - **Role:**
    - **Attitude:**
    - **Decision Making Authority:**
    - **Influence:**
- **Domain Subject Matter Experts**
  1. **John Dela Cruz**
     - **Role:**
     - **Attitude:**
     - **Decision Making Authority:**
     - **Influence:**
- **Implementation Subject Matter Experts**
  1. **Juana Sta. Cruz**
     - **Role:**
     - **Attitude:**
     - **Decision Making Authority:**
     - **Influence:**
- **Operational Support**
- **End Users**
  1. **Something Officers**
     - **Role:**
     - **Attitude:**
     - **Decision Making Authority:**
     - **Influence:**



### Collaboration & Communication Plan

- **Frequency:** Scheduled bi-monthly stakeholder meetings, Impromptu meetings if necessary
- **Available Tools:** Skype for main communications, open-project for project development backlog tracking.
- **What Needs to be Communicated:** Change Requests, Development update,  road-blocks,  requirements management
- **Delivery Method:** Stakeholder meetings will be done in person while immediate concerns can be communicated through the project's skype channel.
- **Level of Detail:** 



------

## Governance Approach

### Decision Making Appointees

- **Impact Analysis -** Johny Dela Cruz
- **Information Reviewer** - Juan Pedro
- **Change Approver-** Juana Dos



### Change Mangement Procedure

#### Change Request Process

  ​	*{ Insert flow/description of procedure of change requests.  How will it will be prioritized, documented,  communicated. Who will do the impact Analysis?  Who will authorize? }*



#### Elements of Change Request

The following factors will be considered when evaluating the requests of business analysis work.

- Cost Estimates
- Benefits
- Risks
- Priority
- Courses of Action



### Prioritization Approach

​	*{ Timelines, expected value, dependencies, resource constraints, adopted methodologies, and other factors influence how requirements and designs are prioritized. }*



### Approval Approach

​	*{ Determines the type of requirements and designs to be approved, the timing for the approvals, the process to follow to gain approval,and who will approve the requirements and designs. Also includes the schedule of events where approvals will occur and how they will be tracked. }*



------

## Information Management Approach

​	The main business analysis documents will be of a markdown document and will be hosted through a private git repository for monitoring and version control, Once approved, these will be converted to PDF format and will be accessed along with other relevant files for the project on a private google drive folder. Flowcharts, diagrams and other charts will be stored in a Microsoft Visio supported file format to enable easy modification and export. 

- The requirements for the project will have the following attributes:
  - [ ] Absolute Reference
  - [ ] Author
  - [ ] Complexity
  - [ ] Ownership
  - [ ] Priority / Urgency
  - [ ] Risks
  - [ ] Source
  - [ ] Stability
  - [ ] Status



### Legal/Regulatory Information

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



> **Republic Act 10173 – Data Privacy Act of 2012**
>
> SEC. 2. Declaration of Policy. – It is the policy of the State to protect the fundamental human right of privacy, of communication while ensuring free flow of information to promote innovation and growth. The State recognizes the vital role of information and



------


## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.