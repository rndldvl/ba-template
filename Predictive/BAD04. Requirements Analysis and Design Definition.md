<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Requirements Analysis and Design Definition v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to ensure that the requirements collectively support one another to fully achieve the objectives, allocate requirements across solution components, and estimate the potential value for each design option and to establish which one is most appropriate to meet the enterprise’s requirements.



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Requirements Analysis and Design Definition and agree with the approach it presents. Any changes to Requirements definitions will be coordinated with and approved by the appointed change management committee stated in the Business Analysis Plan. 

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Functional Requirements

​	Functional requirements capture and specify specific intended behavior of the system being developed. They define things such as system calculations, data manipulation and processing, user interface and interaction with the application, and other specific functionality that show how user requirements are satisfied.



### Usability

- **FR-001** - *{ functional requirements that affect usability such as ease of learning, task efficiency, ease of remembering, understandability, attractiveness, etc. Assign a unique ID number to each requirement }*



### Performance

- **FR-002** - *{ requirements that affect performance such as speed, safety, precision, reliability and availability, capacity, scalability, etc. Assign a unique ID number to each requirement }*



### Supportability

- **FR-003** - *{ requirements that affect supportability and maintainability such as training, ramp-up time, documentation, facilities, etc. Assign a unique ID number to each requirement }*



### Security

- ***FR-004** - { requirements that affect security such as security audits, identification/authentication, privacy, facility access times, etc. Assign a unique ID number to each requirement }*



### Interface

- **FR-005** - *{ requirements that affect interfaces such as user navigation, presentation of application and associated functionality, screen location of interface elements, data display and manipulation, etc. Assign a unique ID number to each requirement }*



## Non-functional Requirements

​	Defines the existing ***non-functional***[^1], technical environment, systems, functions, and processes. Include an overview of the non-functional requirements necessary to achieve the project’s objectives.



### Hardware

- **NFR-001** - *{ requirements and any related processes. Include a detailed description of specific hardware requirements and associate them to specific project functionality/deliverables. Include information such as type of hardware, brand name, specifications, size, security, etc.  }*



### Software

- **NFR-002** - *{ requirements and any related processes. Include a detailed description of specific software requirements and associate them to specific project functionality/deliverables. Include information such as in-house development or purchasing, security, coding language, version numbering, functionality, data, interface requirements, brand name, specifications, etc. }*



### Performance

- **NFR-003** - *{ requirements and any related processes. Include a detailed description of specific performance requirements and associate them to specific project functionality/deliverables. Include information such as cycle time, speed per transaction, test requirements, minimum bug counts, speed, reliability, utilization etc. }*



### Supportability

- **NFR-004** - *{ technical requirements that affect supportability and maintainability such as coding standards, naming conventions, maintenance access, required utilities, etc. }*



### Security

- **NFR-005** - *{ technical requirements that affect security such as security audits, cryptography, user data, system identification/authentication, resource utilization, etc. }*



### Interface

- **NFR-006** - *{ technical requirements that affect interfaces such as protocol management, scheduling, directory services, broadcasts, message types, error and buffer management, security, etc. Assign a unique ID number to each requirement }*



### Availability

- **NFR-007** - *{ technical requirements that affect availability such as hours of operation, level of availability required, down-time impact, support availability, etc. Assign a unique ID number to each requirement }*



## Compliance

- **CR-001** - *{ existing compliance environment as it affects project requirements. Include an overview of the compliance requirements necessary to achieve the project’s objectives }*



## Requirements Architecture

Please refer to the Microsoft Visio file named '*models.vsdx*' for the traceability repository.



## Design Options

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 

- The system must be able to correct records that are erroneously encoded.
  1. Correction done through means of record reversals.
  2. Correction done through record deletion.



## Solution Recommendation

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



### List of Features/Requirements/User Stories

- Feature 1
- Feature 2



### Acceptance Criteria

- Criterion 1
- Criterion 2



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: Also referred to as Quality of Service by the International Institute of Business Analysts, Business Analysis Body of Knowledge.