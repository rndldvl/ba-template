<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Strategy Analysis v1.0* 

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to understand the reasons why an enterprise needs to change some aspect of how it operates and what would be directly or indirectly affected by the change; Determines the set of necessary conditions to meet the business need; To understand the undesirable consequences of internal and external forces on the enterprise during a transition to, or once in, the future state; And to understand the undesirable consequences of internal and external forces on the enterprise during a transition to, or once in, the future state. 



### Approval

​	*{ Approval clause for the document }*

|      Project Sponsor      | Business Analyst |
| :-----------------------: | :--------------: |
| &nbsp;<br /> &nbsp;<br /> |                  |
|      Juan Dela Cruz       | Juana Dela Cruz  |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Current State

### Description

​	*{ The context of the enterprise’s scope, capabilities, resources, performance, culture, dependencies, infrastructure, external influences, and significant relationships between these elements }*

> *'Insert graphs/charts/process flows'*



#### Business Needs

​	*{ Business needs are the problems and opportunities of strategic importance faced by the enterprise. An issue encountered in the organization, such as a customer complaint, a loss of revenue, or a new market opportunity, usually triggers the evaluation of a business need. }*



#### Organizational Structure and Culture

​	*{ Organizational structure defines the formal relationships between people working in the enterprise. Organizational culture is the beliefs, values, and norms shared by the members ofan organization. }*



#### Capabilities and Processes

​	*{ Capabilities and processes describe the activities an enterprise performs. They also include the knowledge the enterprise has, the products and services it provides, the functions it supports, and the methods it uses to make decisions. Capability or Processes Centric. }*



#### Techonology and Infrastructure

​	*{ Information systems used by the enterprise support people in executing processes, making decisions, and in interactions with suppliers and customers. The infrastructure describes the enterprise’s environment with respect to physical components and capabilities. }*



#### Policies

​	*{ They ensure that decisions are made correctly, provide guidance to staff on permitted and appropriate behaviour and actions, support governance, and determine when and how new resources can be acquired. }*



#### Business Architecture

​	*{ How the current state fits in the larger picture within the Enterprise. Business and Stakeholder needs that might overlap with other entities. }*



#### Internal Assets

​	*{ Enterprise assets used in the current state. Resources can be tangible or intangible, such as financial resources, patents, reputation, and brand names. }*



#### External Influencers

​	*{ External influences on the enterprise that do not participate in a change but might present constraints, dependencies, or drivers on the current state. }*



### Business Requirements

​	*{ The problem, opportunity, or constraint which is defined based on an understanding of the current state. }*



------

## Future State


### Business Objectives

​	*{ Business needs are the problems and opportunities of strategic importance faced by the enterprise. An issue encountered in the organization, such as a customer complaint, a loss of revenue, or a new market opportunity, usually triggers the evaluation of a business need. }*



### Description

​	*{ The future state description includes boundaries of the proposed new, removed, and modified components of the enterprise and the potential value expected from the future state. The description might include the desired future capabilities, policies, resources, dependencies, infrastructure, external influences, and relationships between each element. }*

> *'Insert graphs/charts/process flows'*



#### Constraints

​	*{ Constraints describe aspects of the current state, aspects of the planned future state that may not be changed by the solution, or mandatory elements of the design }*



#### Organizational Structure and Culture

​	*{ The elements of the organizational structure and culture may need to change to support the future state }*



#### Capabilities and Processes

​	*{ Identify new kinds of activities or changes in the way activities will be performed to realize the future state. }*



#### Technology and Infrastructure

​	*{ The current technologies and infrastructure that are insufficient to meet the business need and need to change. }*



#### Policies

​	*{ The current policies that are insufficient to meet the business need and need to change.  }*



#### Business Architecture

​	*{ How the future state fits in the larger picture within the desired future state of the whole enterprise. }*



#### Internal Assets

​	*{ Enterprise assets used in the current state of which its resources might need to increase, increased capability, or that new resources may need to be developed. Resources can be tangible or intangible, such as financial resources, patents, reputation, and brand names.  }*



#### Future State Assumptions

​	*{ Assumptions that determine whether the strategy to the future state might work or not. }*



### Potential Value

​	*{ The value that may be realized by implementing the proposed future state. }*



------

## Risk Management

### Process

​	*{ Summarization of steps necessary for responding to project risks, designation of **Risk Manager** }*



### Risk Identification

​	*{ risk management of log }*



### Risk Analysis

#### Qualitative Risk Analysis

| Risk Description | Probability | Impact |
| ---------------- | :---------: | :----: |
|                  |             |        |
|                  |             |        |



#### Quantitative Risk Analysis

​	*{ Analysation of risks prioritized through qualitative risk analysis and how it will affect the project. Numerical ratings will be given on each risk based on anaylsis. }*



### Risk Response Planning



### Risk Monitoring, Controlling, and Reporting
*{*



*Approaches applied to major risks:*

- ***Avoid*** – *eliminate the threat by eliminating the cause*

- ***Mitigate*** – *Identify ways to reduce the probability or the impact of the risk*

- ***Accept*** – *Nothing will be done* 

- ***Transfer*** – *Make another party responsible for the risk (buy insurance, outsourcing, etc.)* 

*}*


### Tools And Practices

​	*{ involved in the risk managemant process }*



------

## Change Strategy

​	*{ The approach that the organization will follow to guide change. }*



### Gap Analysis

​	*{ }*

| Subject | Current State | Future State |
| :-----: | :-----------: | :----------: |
|         |               |              |
|         |               |              |
|         |               |              |





### Enterprise Readiness Assessment

​	*{ }*



## Solution Scope

​	*{ The solution scope that will be achieved through execution of the change strategy. }*



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.