<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Requirements Change Assessment v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to evaluate the implications of proposed changes to requirements and designs.



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Requirements Change Assessment and agree with the approach it presents. Approval of change only entails the allotment of business analysis work needed to accomodate the need for change thus the subject will still have to pass through futher elicitation, refinement, verification and validation before taking effect.

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Design and Requirements Change Assessment

The changes proposed for the date of ***[Date of Requirements Presentation]*** are as follows:



### Change #1 

​	*[ description of the change. ]*

#### Impact Analysis
   - **Benefit:** 
   - **Cost:** 
   - **Impact:** 
   - **Schedule:** 
   - **Urgency:** 



#### Impact Resolution

​	*[ resolutions brought forth by the impact of the change ]*



### Change #2

​	*[ description of the change. ]*

#### Impact Analysis
   - **Benefit:** 
   - **Cost:** 
   - **Impact:** 
   - **Schedule:** 
   - **Urgency:** 



#### Impact Resolution

​	*[ resolutions brought forth by the impact of the change ]*



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.