<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Stakeholder Analysis v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	**For private use of the business analysis group.** The document lists the stakeholders invovled in the project and describes their behavious in relation to the processes involved with the project.



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

### Stakeholder Analysis Log

| ID   | Name | Tile | Stake In Project | Comments |
| ---- | ---- | ---- | ---------------- | -------- |
|      |      |      |                  |          |
|      |      |      |                  |          |



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.