<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Business Analysis Performance Assessment v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	To assess business analysis work and to plan to improve processes where required and to monitor and improve performance. it is necessary to establish the performance measures, conduct the performance analysis, report on the results of the analysis, and identify any necessary preventive, corrective, or developmental actions.



### Approval

​	*{ Approval clause for the document }*

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*




### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Business Analysis Performance Assessment

### Assessment

| Subject                      | Planned | Performance |
| ---------------------------- | ------- | ----------- |
| Accuracy &<br />Completeness |         |             |
| Effectiveness                |         |             |
| Knowledge                    |         |             |
| Significance                 |         |             |
| Strategic                    |         |             |
| Timeliness                   |         |             |



### Recommended Actions for Improvement

The stakeholders deemed the following points for the improvement of the Business Analysis work.

1. ***Lorem ipsum dolor sit amet***, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
2. ***Ut enim ad minim veniam,*** quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.