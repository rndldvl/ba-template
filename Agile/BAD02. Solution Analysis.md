<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Solution Analysis v1.0* 

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to understand the reasons why an enterprise needs to change some aspect of how it operates and what would be directly or indirectly affected by the change; Determines the set of necessary conditions to meet the business need; To understand the undesirable consequences of internal and external forces on the enterprise during a transition to, or once in, the future state; And to understand the undesirable consequences of internal and external forces on the enterprise during a transition to, or once in, the future state. 



### Approval

​	*{ Approval clause for the document }*

|      Project Sponsor      | Business Analyst |
| :-----------------------: | :--------------: |
| &nbsp;<br /> &nbsp;<br /> |                  |
|      Juan Dela Cruz       | Juana Dela Cruz  |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Solution Definition


------




------

## Risk Management

### Process

​	*{ Summarization of steps necessary for responding to project risks, designation of **Risk Manager** }*



### Risk Identification

​	*{ risk management of log }*



### Risk Analysis

#### Qualitative Risk Analysis

| Risk Description | Probability | Impact |
| ---------------- | :---------: | :----: |
|                  |             |        |
|                  |             |        |



#### Quantitative Risk Analysis

​	*{ Analysation of risks prioritized through qualitative risk analysis and how it will affect the project. Numerical ratings will be given on each risk based on anaylsis. }*



### Risk Response Planning



### Risk Monitoring, Controlling, and Reporting
*{*



*Approaches applied to major risks:*

- ***Avoid*** – *eliminate the threat by eliminating the cause*

- ***Mitigate*** – *Identify ways to reduce the probability or the impact of the risk*

- ***Accept*** – *Nothing will be done* 

- ***Transfer*** – *Make another party responsible for the risk (buy insurance, outsourcing, etc.)* 

*}*


### Tools And Practices

​	*{ involved in the risk managemant process }*



------

## Change Strategy

​	*{ The approach that the organization will follow to guide change. }*



### Gap Analysis

​	*{ }*

| Subject | Current State | Future State |
| :-----: | :-----------: | :----------: |
|         |               |              |
|         |               |              |
|         |               |              |





### Enterprise Readiness Assessment

​	*{ }*



## Solution Scope

​	*{ The solution scope that will be achieved through execution of the change strategy. }*



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.