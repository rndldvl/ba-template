<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Requirements Analysis and Design Definition v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to ensure that the requirements collectively support one another to fully achieve the objectives, allocate requirements across solution components, and estimate the potential value for each design option and to establish which one is most appropriate to meet the enterprise’s requirements.



### Approval

​	*{ Approval clause for the document }*

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Requirements Traceability Matrix

| BR. ID | BR. Desc. | FR. ID | FR. Desc. | TC. ID | Test Exec. | UAT Exec. | Prod Exec. | Defect IDs |
| ------ | --------- | ------ | --------- | ------ | ---------- | --------- | ---------- | ---------- |
|        |           |        |           |        |            |           |            |            |
|        |           |        |           |        |            |           |            |            |
|        |           |        |           |        |            |           |            |            |



## Test Cases





## Defects





------

## Appendix

### Glossary
- **B**
   - **BR** - Stands for ***'Business Requirement'***.
- **E**
   - **Exec** - Stands for ***'Execution'***. Often specifies on what enviroment the test was executed.
- **F**
   - **FR** - stands for ***'Functional Requirement'***
- **T**
   - **TC** - Stands for ***'Test Case'***



### Footnotes
[^1]: First footnote.