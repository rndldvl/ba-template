<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Solution Evaluation v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	This document defines performance measures and use the data collected to evaluate the effectiveness of a solution in relation to the value it brings. The document also determines the factors external and internal to the solution that restrict the full realization of value and recommend course of actions to align them.



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Solution Evaluation and agree with the approach it presents. Any changes to solution evaluation will be coordinated with and approved by the appointed change management committee stated in the Business Analysis Plan. 

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*




### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Solution Performance Measures

The following data is from the date range of September 1 to 7, 2019.



### Quantitative Measures

| Subject                                 | Desired       | Performance   |
| --------------------------------------- | ------------- | ------------- |
| Records Processed per day (avg.)        | 50,000 pieces | 45,678 pieces |
| Records Error/Failures per day (avg.)   | 50 pieces     | 1,234 pieces  |
| Record Processing time (system average) | 10ms          | 18ms          |



### Qualitative Measures

Stakeholder Feedbacks

- **End-User**
  - Interface lags from time to time
  - Noticeably slows down through continous operations
- **Implementation Subject Matter Expert**
  - Volume of records beyond expectations



------

## Solution Performance Analysis

​	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



------

## Solution Limitation

- **Branch Internet Speed** - The municipal and provincial branches' connection speed can not satisfy the amount of bandwidth needed by the solution.
- **Branch PC Specifications** - In due time, the branches' local database becomes too populated which in turn draws affects the performance of the PC and affecting everything else.



------

## Enterprise Limitation

- **Associates with Unrelated Degree** - Branch level hiring excludes the degree as basis of selection hence leading to difficulty in understanding and following the concepts and procedures relating to the product.
- **Love of Familiarity** - End-User group ***X*** is unable to maximize the value of the solution due to being unable to cope with change and clinging to past procedures.



------

## Recommended Actions

- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
- Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.