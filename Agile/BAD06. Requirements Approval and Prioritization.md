<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Requirements Approval and Prioritization v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The document serves to obtain agreement on and approval of requirements and designs for business analysis work to continue and/or solution construction to proceed and to rank requirements in the order of relative importance.



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Requirements Approval and Prioritization and agree with the approach it presents. Any changes to the approved and prioritized requirements will be coordinated with and approved by the appointed change management committee stated in the Business Analysis Plan. 

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Approved Designs and Requirements

​	The undersigned acknowledge they have reviewed the ***Project Name*** Requirements Management Plan and agree with the approach it presents. Changes to this Requirements Management Plan will be coordinated with and approved by the undersigned or their designated representatives.



- Requirements Approved on August 24, 2019
  - Requirement #1
  - Requirement #5
- Requirements approved on August 28, 2019
  - Requirement #4
  - Requirement #2



## Requirements Prioritization

- **First Iteration**
  - Requirement #1
  - Requirement #4
  - Requirement #5
- **Second Iteration**
  - Requirement #2
  - Requirement #3



## Design and Requirements Change Assessment

No change is deemed necessary for the current phase of the project.



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.