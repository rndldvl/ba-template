<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Elicitation Results v1.0 - 01/01/2001*

---

[TOC]

---



## Document Information

### Purpose

​	The document determines the scope of the elicitation activity, select appropriate techniques and contains the results of the elicitation activity.



### Approval

​	*{ Approval clause for the document }*

|     Business Analyst      |   Stakeholder   | Stakeholder 2 |  Stakeholder 3  |
| :-----------------------: | :-------------: | :-----------: | :-------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                 |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise | Wan Dila Kuruzu |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Elicitation and Collaboration

### Elicitation Activity Plans

- **Schedule: **August 17, 2019 2:00pm @ IT Mini Conference Room

- **Goals:** Initial Requirements Elicitation

- **Participants:** Domain Subject Matter Experts, Project Sponsor, End Users

- **Supporting Materials:** Departmental Manual, BCP

- **Techniques to be Used:** Brainstorming, Concept and Process Modelling, Mind Mapping

  

### Elicitation Results

**August 17, 2019**

| Results | Status |
| ---------- | :-----: |
| User Authentication and Logging | Confirmed |
| Something something interface something | Unconfirmed |



------

## Appendix
### Glossary

- **A**
  - ***Alphabet -*** collection of letters in the english language.



### Footnotes
[^1]: First footnote.