<style type="text/css">@page { size: letter; margin: 0.025in; }</style>
# Project Name

*Solution Glossary v1.0*

------

[TOC]

------

## Document Information

### Purpose

​	The Document serves as reference for all the jargon, keywords, and terminologies used within the project.



### Approval

​	The undersigned acknowledge they have reviewed the ***Project Name*** Solution Glossary and agree with the definitions it presents. Changes to the Solution Glossary will be coordinated with and approved by the undersigned or their designated representatives.

|     Business Analyst      | Project Sponsor |  Domain SME   | Implementation SME |
| :-----------------------: | :-------------: | :-----------: | :----------------: |
| &nbsp;<br /> &nbsp;<br /> |                 |               |                    |
|      Juan Dela Cruz       | Juana Dela Cruz | John D Cruise |  Wan Dila Kuruzu   |



### Distribution

​	*{ Notes regarding the distribution and version of the document }*

| Version | Implemented By | Revision Date | Approved By | Approval Date |                          Reason                          |
| :-----: | :------------: | :-----------: | :---------: | :-----------: | :------------------------------------------------------: |
|   1.0   |   01/01/2019   |  01/01/2019   | 01/01/2019  |  01/01/2019   | Lorem ipsum dolor sit amet, consectetur adipiscing elit. |
|         |                |               |             |               |                                                          |
|         |                |               |             |               |                                                          |



### Referenced Documents

​	*{ List of documents referenced in any part of the this document and how references are used }*



### Assumptions

​	*Assumptions made in this document*



### Constraints	

​	*Constraints encountered drafting the document*



------

## Solution Name

### Solution Component/Module

#### Function Area #1





#### Function Area #2





------

## Solution #2 Name 

### Solution Component/Module

#### Function Area #1



#### Function Area #2





------

## Appendix
### Glossary

- *S*
  - ***SME-*** abbreviation of ***'Subject Matter Expert'***.

### Footnotes
[^1]: First footnote.

